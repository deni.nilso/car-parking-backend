const router = require('express').Router();
let Park = require('../models/parks.model');

router.route('/').get((req, res) => {
    Park.find()
        .then(parks => res.json(parks))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/add').post((req,res) => {
    const parkname = req.body.parkname;
    const parkinglots = Number(req.body.parkinglots);
    const parkinglotsavailable = Number(req.body.parkinglotsavailable);

    const newPark = new Park({parkname,parkinglots,parkinglotsavailable});

    newPark.save()
        .then(() => res.json('Estacionamento adicionado!'))
        .catch(err => res.status(400).json('Erro: ' + err));
});

router.route('/:id').get((req, res) => {
    Park.findById(req.params.id)
        .then(park => res.json(park))
        .catch(err => res.status(400).json('Erro: ' + err))
});

router.route('/:id').delete((req, res) => {
    Park.findByIdAndDelete(req.params.id)
        .then(park => res.json('Estacionamento apagado.'))
        .catch(err => res.status(400).json('Erro: ' + err))
});

router.route('/update/:id').post((req, res) => {
    Park.findByIdAndUpdate(req.params.id)
        .then(park => {
            park.parkname = req.body.parkname;
            park.parkinglots = Number(req.body.parkinglots);
            park.parkinglotsavailable = Number(req.body.parkinglotsavailable);

            park.save()
                .then(() => res.json('Estacionamento atualizado!'))
                .catch(err => res.status(400).json('Erro :' + err));
        })
        .catch(err => res.status(400).json('Erro :' + err))
});

router.route('/fill/:id').post((req, res) => {
    Park.findByIdAndUpdate(req.params.id)
        .then(park => {
            if(Number(park.parkinglotsavailable)>0){
                park.parkinglotsavailable = Number(park.parkinglotsavailable) - 1;
            }

            park.save()
                .then(() => res.json('Vaga preenchida! Total disponivel agora e: ' + park.parkinglotsavailable))
                .catch(err => res.status(400).json('Erro :' + err));
        })
        .catch(err => res.status(400).json('Erro :' + err))
});

router.route('/free/:id').post((req, res) => {
    Park.findByIdAndUpdate(req.params.id)
        .then(park => {
            if(Number(park.parkinglotsavailable)<park.parkinglots){
                park.parkinglotsavailable = Number(park.parkinglotsavailable) + 1;
            }
            
            park.save()
                .then(() => res.json('Vaga liberada! Total disponivel agora e: ' + park.parkinglotsavailable))
                .catch(err => res.status(400).json('Erro :' + err));
        })
        .catch(err => res.status(400).json('Erro :' + err))
});

module.exports = router;