const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const parkSchema = new Schema({
    parkname: {
       type: String,
       required: true,
       unique: true,
       trim: true,
       minlength: 3 
    },
    parkinglots: { type: Number, required: true},
    parkinglotsavailable: { type: Number, required: true},
}, {
    timestamps: true,
});

const Park = mongoose.model('Park', parkSchema);

module.exports = Park;